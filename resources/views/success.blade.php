@extends('layouts.app')

@section('title')
    Order success
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="success-container">
                <div>
                    <h2><strong>Risultati della ricerca</strong></h2>
                    <h5>Da Como a Roma - Peso: 20kg</h5>
                </div>

                <div class="success-details">
                    <p>Numbero spedizione: #003241</p>
                    <p>Data di ritiro: 12/09/2019</p>
                    <p>Referente: Mario Rossi</p>
                </div>

                <h5><a href="javacript:void(0)">Crea un account e conserva lo storico delle spedizioni</a></h5>
                <a href="{{ url('/') }}" class="submit-button btn btn-success">Torna alla homepage</a>
            </div>
        </div>
    </div>
</div>
@endsection
