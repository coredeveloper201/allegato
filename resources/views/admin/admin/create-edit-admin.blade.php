@extends('admin.layouts.app')

@section('title')
    Admin create
@endsection

@section('heading')
    Admin @if(isset($edit)) edit @else create @endif
@endsection

@section('breadcrumb')
    Admin @if(isset($edit)) edit @else create @endif
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! html_entity_decode(Session::get('success')) !!}
        </div>
    @endif
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">@if(isset($edit)) Edit @else Create @endif admin</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('admins.index') }}" class="btn btn-info"><i class="mdi mdi-step-backward"></i> Back to list</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <form method="post" action="@if(isset($edit)) {{ route('admins.update', $admin->id) }} @else {{ route('admins.store') }} @endif" novalidate enctype="multipart/form-data">
                        @csrf
                        @if(isset($edit))
                            @method('PUT')
                        @endif
                        <div class="row">
                            <div class="col-lg-12 col-md-12">

                                <div class="form-group">
                                    <h5>Name<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="name" value="@if(isset($edit)){{ $admin->name }}@else{{ old('name') }}@endif" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" required data-validation-required-message="Please provide a name for the admin">
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Email @if (!isset($edit))<span class="text-danger">*</span>@endif</h5>
                                    <div class="controls">
                                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" placeholder="E-mail"  @if (!isset($edit))required data-validation-required-message="Please provide a email for the admin."@endif>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Image</h5>
                                    <div class="controls">
                                        @if(isset($edit)) @if ($admin->avatar)<img src="{{ asset('storage/images/profile_images/thumbnail/'.$admin->avatar) }}" height="100px" width="100px"> @endif @endif
                                        <input type="file" name="avatar" class="form-control{{ $errors->has('avatar') ? ' is-invalid' : '' }}" accept="image/*">
                                        @error('avatar')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Password @if (!isset($edit))<span class="text-danger">*</span>@endif</h5>
                                    <div class="controls">
                                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}"  autocomplete="password" placeholder="Password" @if (!isset($edit))required data-validation-required-message="Please provide a password for the admin."@endif>

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                @if (!isset($edit))
                                <div class="form-group">
                                    <h5>Confirm Password<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="password" class="form-control" name="password_confirmation" required autocomplete="confirm-password" placeholder="Confirm password" data-validation-required-message="Please confirm password.">
                                    </div>
                                </div>
                                @endif

                                <div class="form-group">
                                    <h5>Admin role<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <select class="form-control" name="role_id" required data-validation-required-message="Please select a role.">
                                            <option value="">Please select a role</option>
                                            @foreach($roles as $role)
                                            <option value="{{ $role->id }}" @if (isset($edit)) @foreach ($admin->roles as $admin_role)@endforeach @if ($role->id == $admin_role->id) selected @endif @endif>{{ $role->title }}</option>
                                            @endforeach
                                        </select>

                                        @error('role_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-12 col-md-12">
                                <hr>
                                <div class="text-xs-right">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-inverse">Reset</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
