@extends('admin.layouts.app')

@section('title')
    Account settings
@endsection

@section('heading')
    Account settings
@endsection

@section('breadcrumb')
    Account settings
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! html_entity_decode(Session::get('success')) !!}
        </div>
    @endif
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title text-center">Update account details</h4>
                    <hr>
                    <form method="post" action="{{ route('admin.account.update', $admin->id) }}" novalidate enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="old_email" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}<span class="text-danger">*</span></label>

                            <div class="col-md-6 controls">
                                <input type="text" name="name" value="{{ $admin->name }}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" required data-validation-required-message="Please provide a name for the admin">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="old_email" class="col-md-4 col-form-label text-md-right">{{ __('Image') }}</label>

                            <div class="col-md-6 controls">
                                @if ($admin->avatar)<img src="{{ asset('storage/images/profile_images/thumbnail/'.$admin->avatar) }}" height="100px" width="100px"> @endif
                                <input type="file" name="avatar" class="form-control{{ $errors->has('avatar') ? ' is-invalid' : '' }}" accept="image/*">
                                @error('avatar')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="submit" class="col-md-4 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <button class="btn btn-primary" type="submit">Submit form</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <h4 class="card-title text-center">Update email</h4>
                    <hr>
                    <form method="post" action="{{ route('admin.email.update') }}" novalidate>
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group row">
                            <label for="old_email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}<span class="text-danger">*</span></label>

                            <div class="col-md-6 controls">
                                <input type="email" class="form-control @error('old_email') is-invalid @enderror @if(Session::has('error_email'))  is-invalid  @endif" name="old_email" autocomplete="email" placeholder="Your old email" required data-validation-required-message="Please provide your old email.">

                                @error('old_email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                @if(Session::has('error_email'))
                                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ Session::get('error_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('New Email') }}<span class="text-danger">*</span></label>

                            <div class="col-md-6 controls">
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" required autocomplete="new-email" placeholder="New email" data-validation-required-message="Please provide a new email.">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Email') }}<span class="text-danger">*</span></label>

                            <div class="col-md-6 controls">
                                <input type="email" class="form-control" name="email_confirmation" required autocomplete="new-email" placeholder="Confirm new email" data-validation-required-message="Please confirm new email.">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="submit" class="col-md-4 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <button class="btn btn-primary" type="submit">Submit form</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <h4 class="card-title text-center">Update password</h4>
                    <hr>
                    <form method="post" action="{{ route('admin.password.reset') }}" novalidate>
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}<span class="text-danger">*</span></label>

                            <div class="col-md-6 controls">
                                <input type="password" class="form-control @error('password') is-invalid @enderror @if(Session::has('error'))  is-invalid  @endif" name="password" autocomplete="new-password" placeholder="Your old password" required data-validation-required-message="Please provide your old password.">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                @if(Session::has('error'))
                                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ Session::get('error') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="new_password" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}<span class="text-danger">*</span></label>

                            <div class="col-md-6 controls">
                                <input type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" required autocomplete="new-password" placeholder="New password" data-validation-required-message="Please provide a new password.">

                                @error('new_password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}<span class="text-danger">*</span></label>

                            <div class="col-md-6 controls">
                                <input type="password" class="form-control" name="new_password_confirmation" required autocomplete="new-password" placeholder="Confirm new password" data-validation-required-message="Please confirm new password.">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="submit" class="col-md-4 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <button class="btn btn-primary" type="submit">Submit form</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


