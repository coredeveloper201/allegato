@extends('admin.layouts.app')

@section('title')
    Admins
@endsection

@section('heading')
    Admins
@endsection

@section('breadcrumb')
    Admins
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! html_entity_decode(Session::get('success')) !!}
        </div>
    @endif
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="material-card card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">Admins</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('admins.create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add New</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-hover border display" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($admins as $admin)
                                <tr>
                                    <td>{{ $admin->name }}</td>
                                    <td><img src="{{ asset('storage/images/profile_images/thumbnail/'.$admin->avatar) }}" v-if="avatar" style="border-radius: 50%" width="36"></td>
                                    <td>{{ $admin->email }}</td>
                                    @foreach ($admin->roles as $admin_role)@endforeach
                                    <td>{{ $admin_role->title }}</td>
                                    <td>
                                        <form action="{{ route('admins.destroy', $admin->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <a href="{{ route('admins.edit', $admin->id)}}" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i> </a>
                                            <button type="submit" onclick="return confirm('Are you sure want to delete this data?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
