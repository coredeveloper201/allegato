@extends('admin.layouts.app')

@section('title')
    Invoices
@endsection

@section('heading')
    Invoices
@endsection

@section('breadcrumb')
    Invoices
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! html_entity_decode(Session::get('success')) !!}
        </div>
    @endif
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="material-card card">
                <div class="card-body">
                    <h4 class="card-title">Invoices</h4>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-hover border display" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Order No</th>
                                <th>Customer Name</th>
                                <th>Balance</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($invoices as $invoice)
                                <tr>
                                    <td>{{ $invoice->created_at }}</td>
                                    <td>{{ $invoice->order->id }}</td>
                                    <td>{{ $invoice->order->user->name }}</td>
                                    <td>€ {{ $invoice->order->price }}</td>
                                    <td>
                                        <form action="https://www.app.fattura24.com/api/v0.3/GetFile" id="invoices-download" method="post">
                                            @csrf
                                            <input type="hidden" name="apiKey" value="{{ env('FATTURA24_API_KEY') }}">
                                            <input type="hidden" name="docId" value="{{ $invoice->docId }}">
                                        </form>
                                        <form action="{{ route('invoices.destroy', $invoice->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <a href="{{ route('invoices.show', $invoice->id)}}" class="btn btn-info btn-circle"><i class="fa fa-eye"></i> </a>
                                            <a onclick="event.preventDefault();document.getElementById('invoices-download').submit();" href="javascript:void(0)" class="btn btn-primary btn-circle"><i class="fas fa-file-pdf"></i> </a>
                                            <button type="submit" onclick="return confirm('Are you sure want to delete this data?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
