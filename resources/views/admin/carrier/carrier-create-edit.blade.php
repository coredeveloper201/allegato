@extends('admin.layouts.app')

@section('title')
    Courier create
@endsection

@section('heading')
    Courier @if(isset($edit)) edit @else create @endif
@endsection

@section('breadcrumb')
    Courier @if(isset($edit)) edit @else create @endif
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! html_entity_decode(Session::get('success')) !!}
        </div>
    @endif
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">@if(isset($edit)) Edit @else Create @endif courier</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('carriers.index') }}" class="btn btn-info"><i class="mdi mdi-step-backward"></i> Back to list</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <form method="post" action="@if(isset($edit)) {{ route('carriers.update', $carrier->id) }} @else {{ route('carriers.store') }} @endif" novalidate enctype="multipart/form-data">
                        @csrf
                        @if(isset($edit))
                            @method('PUT')
                        @endif
                        <div class="row">
                            <div class="col-lg-12 col-md-12">

                                <div class="form-group">
                                    <h5>Title<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="title" value="@if(isset($edit)){{ $carrier->title }}@else{{ old('title') }}@endif" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @if ($errors->has('title'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Logo @if(!isset($edit))<span class="text-danger">*</span>@endif</h5>
                                    <div class="controls">
                                        @if(isset($edit)) <img src="{{ asset('storage/images/logos/'.$carrier->logo) }}" height="100px" width="100px"> @endif
                                        <input type="file" accept="image/*" name="logo" class="form-control{{ $errors->has('logo') ? ' is-invalid' : '' }}" @if(!isset($edit)) required data-validation-required-message="This field is required" @endif>
                                        @if ($errors->has('logo'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('logo') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>App key<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="app_key" value="@if(isset($edit)){{ $carrier->app_key }}@else{{ old('app_key') }}@endif" class="form-control{{ $errors->has('app_key') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @if ($errors->has('app_key'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('app_key') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>App secret<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="secret_key" value="@if(isset($edit)){{ $carrier->secret_key }}@else{{ old('secret_key') }}@endif" class="form-control{{ $errors->has('secret_key') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required.">
                                        @if ($errors->has('secret_key'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('secret_key') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-12 col-md-12">
                                <hr>
                                <div class="text-xs-right">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-inverse">Reset</button>
                                </div>
                            </div>

                            <div class="col-12">
                                <br>
                                <hr>
                            </div>
                        </div>
                    </form>
                    @if (!isset($edit))
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title text-info">Alternately you can create courier by uploading excel sheet</h4>
                        </div>
                        <div class="col-md-2 text-right">

                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <form method="post" action="{{ route('carriers.store') }}" novalidate enctype="multipart/form-data">
                        @csrf
                        @if(isset($edit))
                            @method('PUT')
                        @endif
                        <div class="row">
                            <div class="col-lg-12 col-md-12">

                                <div class="form-group">
                                    <h5>Choose excel sheet<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="file" name="carriers" accept=".csv,.xlsx,.xls" value="{{ old('carriers') }}" class="form-control{{ $errors->has('carriers') ? ' is-invalid' : '' }}" required data-validation-required-message="Please choose a excel file to import courier.">
                                        @if ($errors->has('carriers'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('carriers') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-12 col-md-12">
                                <hr>
                                <div class="text-xs-right">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-inverse">Reset</button>
                                </div>
                            </div>

                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
