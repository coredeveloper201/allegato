@extends('admin.layouts.app')

@section('title')
    Courier show
@endsection

@section('heading')
    Courier show
@endsection

@section('breadcrumb')
    Courier show
@endsection

@section('content')
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">{{ $carrier->title }}</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('carriers.index') }}" class="btn btn-info"><i class="mdi mdi-step-backward"></i> Back to list</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="file_export" class="table table-striped table-hover border display" style="width: 100%">
                            <tbody>
                                <tr>
                                    <th>Title</th>
                                    <td>{{ $carrier->title }}</td>
                                </tr>
                                <tr>
                                    <th>Logo</th>
                                    <td><img src="{{ asset('storage/images/logos/'.$carrier->logo) }}"></td>
                                </tr>
                                <tr>
                                    <th>App key</th>
                                    <td>{{ $carrier->app_key }}</td>
                                </tr>
                                <tr>
                                    <th>Secret key</th>
                                    <td>{{ $carrier->secret_key }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
