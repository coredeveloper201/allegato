@extends('admin.layouts.app')

@section('title')
    Couriers
@endsection

@section('heading')
    Couriers
@endsection

@section('breadcrumb')
    Couriers
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! html_entity_decode(Session::get('success')) !!}
        </div>
    @endif
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="material-card card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">Couriers</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('carriers.create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add New</a>
                            <a href="{{ route('carriers.download') }}" class="btn btn-primary"><i class="fas fa-file-excel"></i> Export</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-hover border display" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Logo</th>
                                <th>App key</th>
                                <th>Secret key</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($carriers as $carrier)
                                <tr>
                                    <td>{{ $carrier->title }}</td>
                                    <td><img src="{{ asset('storage/images/logos/'.$carrier->logo) }}" height="100px" width="100px"></td>
                                    <td>**************</td>
                                    <td>**************</td>
                                    <td>
                                        <form action="{{ route('carriers.destroy', $carrier->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <a href="{{ route('carriers.show', $carrier->id)}}" class="btn btn-info btn-circle"><i class="fa fa-eye"></i> </a>
                                            <a href="{{ route('carriers.edit', $carrier->id)}}" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i> </a>
                                            <button type="submit" onclick="return confirm('Are you sure want to delete this data?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
