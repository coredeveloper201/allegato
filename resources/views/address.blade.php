@extends('layouts.app')

@section('title')
    Address
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="address-container">
                <div>
                    <h2><strong>Risultati della ricerca</strong></h2>
                    <h5>Da Como a Roma - Peso: 20kg</h5>
                </div>

                <form class="calculation-form needs-validation" action="{{ route('payment') }}" method="get" novalidate>
                    <input type="hidden" name="" value="{{ $request }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="address-half">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <h5 class="text-uppercase float-left"><strong>mittente</strong></h5>
                                        <label class="switch float-right">
                                            <input type="checkbox">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <a href="javascript:void(0)">Non hai un account? Crealo qui</a>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-10">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>

                                    <div class="form-group col-md-2">
                                        <label for="formGroupExampleInput">&nbsp;</label><br>
                                        <button type="button" class="btn btn-success float-right"><i class="fas fa-info-circle"></i></button>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="address-half">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <h5 class="text-uppercase float-left"><strong>destinatario</strong></h5>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-10">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>

                                    <div class="form-group col-md-2">
                                        <label for="formGroupExampleInput">&nbsp;</label><br>
                                        <button type="button" class="btn btn-success float-right"><i class="fas fa-info-circle"></i></button>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="formGroupExampleInput">Example label</label>
                                        <input type="text" class="form-control" placeholder="First name" required>
                                        <div class="invalid-tooltip">
                                            This field is required.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="formGroupExampleInput"></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="submit-button-container float-right"><button type="submit" class="submit-button btn btn-success">Continue</button></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
