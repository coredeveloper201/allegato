@extends('layouts.app')

@section('title')
    Couriers
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="courier-container">
                <div>
                    <h2><strong>Risultati della ricerca</strong></h2>
                    <h5>Da Como a Roma - Peso: 20kg</h5>
                </div>

                <div>
                    <div class="filter-container">
                        <form>
                            <div class="form-row align-items-center">
                                <div class="col-auto">
                                    <select class="form-control">
                                        <option>Filtra risultati del preventivo</option>
                                    </select>
                                </div>
                                <div class="col-auto">
                                    <select class="form-control">
                                        <option>Ordina i risultati del preventivo</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Tipo di servizio</th>
                                <th>Stampante richiesta?</th>
                                <th>Informazioni</th>
                                <th>Prezzo da</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($couriers as $courier)
                            <tr class="courier-table-first-row">
                                <td>PREFERITO</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td rowspan="2">
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                            <h5><strong>24,36 €</strong></h5>
                                            <small>29,23 € Inc. IVA</small>
                                        </div>
                                        <div class="col-md-6">
                                            <form method="post" action="{{ route('carts.store') }}">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $courier->id }}">
                                                <input type="hidden" name="product" value="{{ $courier->title }}">
                                                <input type="hidden" name="price" value="23">
                                                <input type="hidden" name="type" value="package">
                                                <input type="hidden" name="weight" value="{{ $request->weight }}">
                                                <input type="hidden" name="length" value="2">
                                                <input type="hidden" name="width" value="2">
                                                <input type="hidden" name="height" value="2">
                                                <input type="hidden" name="location_from" value="{{ $request->location_from }}">
                                                <input type="hidden" name="location_to" value="{{ $request->location_to }}">
                                                <button type="submit" class="submission btn btn-lg"><b>PRENOTA</b></button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="courier-table-second-row">
                                <td><img src="{{ asset('images/logos/courier-logo.png') }}" width="140" height="54"></td>
                                <td>
                                    <h5><strong>Bartolini Standard</strong></h5>
                                    <small>Tra 1 e 2 giorni di consegna</small>
                                </td>
                                <td>
                                    <h5><strong>Ritirato da casa o dal luogo di lavoro</strong></h5>
                                    <small>Consegnato a casa o sul luogo di lavoro</small>
                                </td>
                                <td><i class="fa fa-print"></i></td>
                                <td><i class="fa fa-info-circle" onclick="showInfo()"></i></td>
                            </tr>
                            <tr class="courier-table-third-row">
                                <td colspan="2">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    (89 recensioni)
                                </td>
                                <td colspan="4">Copertura assicurativa opzionale disponibile fino a (89 recensioni) un valore di 1.000,00 €.</td>
                            </tr>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            @endforeach

                            <tr class="courier-table-first-row">
                                <td>PREFERITO</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td rowspan="2">
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                            <h5><strong>24,36 €</strong></h5>
                                            <small>29,23 € Inc. IVA</small>
                                        </div>
                                        <div class="col-md-6">
                                            <button class="submission btn btn-lg"><b>PRENOTA</b></button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="courier-table-second-row">
                                <td><img src="{{ asset('images/logos/courier-logo-2.png') }}" width="140" height="54"></td>
                                <td>
                                    <h5><strong>Bartolini Standard</strong></h5>
                                    <small>Tra 1 e 2 giorni di consegna</small>
                                </td>
                                <td>
                                    <h5><strong>Ritirato da casa o dal luogo di lavoro</strong></h5>
                                    <small>Consegnato a casa o sul luogo di lavoro</small>
                                </td>
                                <td><i class="fa fa-print"></i></td>
                                <td><i class="fa fa-info-circle" onclick="showInfo()"></i></td>
                            </tr>
                            <tr class="courier-table-third-row">
                                <td colspan="2">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    (89 recensioni)
                                </td>
                                <td colspan="4">Copertura assicurativa opzionale disponibile fino a (89 recensioni) un valore di 1.000,00 €.</td>
                            </tr>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>

                            <tr class="courier-table-first-row">
                                <td>PREFERITO</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td rowspan="2">
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                            <h5><strong>24,36 €</strong></h5>
                                            <small>29,23 € Inc. IVA</small>
                                        </div>
                                        <div class="col-md-6">
                                            <button class="submission btn btn-lg"><b>PRENOTA</b></button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="courier-table-second-row">
                                <td><img src="{{ asset('images/logos/courier-logo-3.png') }}" width="140" height="54"></td>
                                <td>
                                    <h5><strong>Bartolini Standard</strong></h5>
                                    <small>Tra 1 e 2 giorni di consegna</small>
                                </td>
                                <td>
                                    <h5><strong>Ritirato da casa o dal luogo di lavoro</strong></h5>
                                    <small>Consegnato a casa o sul luogo di lavoro</small>
                                </td>
                                <td><i class="fa fa-print"></i></td>
                                <td><i class="fa fa-info-circle" onclick="showInfo()"></i></td>
                            </tr>
                            <tr class="courier-table-third-row">
                                <td colspan="2">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    (89 recensioni)
                                </td>
                                <td colspan="4">Copertura assicurativa opzionale disponibile fino a (89 recensioni) un valore di 1.000,00 €.</td>
                            </tr>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>

                            <tr class="courier-table-first-row">
                                <td>PREFERITO</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td rowspan="2">
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                            <h5><strong>24,36 €</strong></h5>
                                            <small>29,23 € Inc. IVA</small>
                                        </div>
                                        <div class="col-md-6">
                                            <button class="submission btn btn-lg"><b>PRENOTA</b></button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="courier-table-second-row">
                                <td><img src="{{ asset('images/logos/courier-logo-4.png') }}" width="140" height="54"></td>
                                <td>
                                    <h5><strong>Bartolini Standard</strong></h5>
                                    <small>Tra 1 e 2 giorni di consegna</small>
                                </td>
                                <td>
                                    <h5><strong>Ritirato da casa o dal luogo di lavoro</strong></h5>
                                    <small>Consegnato a casa o sul luogo di lavoro</small>
                                </td>
                                <td><i class="fa fa-print"></i></td>
                                <td><i class="fa fa-info-circle" onclick="showInfo()"></i></td>
                            </tr>
                            <tr class="courier-table-third-row">
                                <td colspan="2">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    (89 recensioni)
                                </td>
                                <td colspan="4">Copertura assicurativa opzionale disponibile fino a (89 recensioni) un valore di 1.000,00 €.</td>
                            </tr>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>

                            <tr class="courier-table-first-row">
                                <td>PREFERITO</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td rowspan="2">
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                            <h5><strong>24,36 €</strong></h5>
                                            <small>29,23 € Inc. IVA</small>
                                        </div>
                                        <div class="col-md-6">
                                            <button class="submission btn btn-lg"><b>PRENOTA</b></button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="courier-table-second-row">
                                <td><img src="{{ asset('images/logos/courier-logo-5.png') }}" width="140" height="54"></td>
                                <td>
                                    <h5><strong>Bartolini Standard</strong></h5>
                                    <small>Tra 1 e 2 giorni di consegna</small>
                                </td>
                                <td>
                                    <h5><strong>Ritirato da casa o dal luogo di lavoro</strong></h5>
                                    <small>Consegnato a casa o sul luogo di lavoro</small>
                                </td>
                                <td><i class="fa fa-print"></i></td>
                                <td><i class="fa fa-info-circle" onclick="showInfo()"></i></td>
                            </tr>
                            <tr class="courier-table-third-row">
                                <td colspan="2">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    (89 recensioni)
                                </td>
                                <td colspan="4">Copertura assicurativa opzionale disponibile fino a (89 recensioni) un valore di 1.000,00 €.</td>
                            </tr>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="courier-loader" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><strong>Sto cercando le migliori offerte</strong></h4>
                                <p id="progressCounter">60%</p>
                            </div>
                            <div class="loader modal-body">
                                <div class="text-center">
                                    <img src="{{ asset('images/site/box.png') }}">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="courier-show" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><strong>Più informazioni</strong></h4>
                                <button type="button" class="close" data-dismiss="modal"><strong>&times;</strong></button>
                            </div>
                            <div class="modal-body">
                                <ul>
                                    <li>Tra 2 e 4 giorni feriali di consegna.</li>
                                    <li>Monitoraggio completo per la consegna.</li>
                                    <li>Stampa un'etichetta a casa.</li>
                                    <li>Copertura assicurativa opzionale disponibile fino a un valore di 1.000,00 €.</li>
                                    <li>Raccolto dall'indirizzo di casa o di lavoro.</li>
                                    <li>Prenota prima delle 11:30 per il ritiro in giornata.</li>
                                    <li>Collezioni effettuati fino 17:30 nei giorni feriali.</li>
                                    <li>Consegna dal lunedì al venerdì prima delle 17:30.</li>
                                    <li>Peso massimo 68 kg.</li>
                                    <li>Si prega di fornire sempre un numero contatto locale per il destinatario.</li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

