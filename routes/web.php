<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

// Home route goes here
Route::get('/', 'HomeController@index')->name('home');

// User routes go here
Route::get('dashboard', 'User\UserController@index')->name('dashboard');
Route::put('users/{user}', 'User\UserController@update')->name('users.update');
Route::put('users-psw-update', 'User\UserController@updatePassword')->name('user.password.update');
Route::put('users-upload-image', 'User\UserController@uploadImage')->name('users.image.upload');
Route::delete('users/{user}', 'User\UserController@destroy')->name('users.destroy');
Route::post('addresses', 'AddressController@store')->name('addresses.store');
Route::put('addresses/{address}', 'AddressController@update')->name('addresses.update');
Route::delete('addresses/{address}', 'AddressController@destroy')->name('addresses.destroy');

// Calculation route goes here
Route::get('calculate', 'SiteController@calculate')->name('calculate');
Route::get('address', 'SiteController@address')->name('address.get');
Route::get('payment', 'SiteController@payment')->name('payment');
Route::get('order/complete', 'SiteController@orderSuccess')->name('order.success');
Route::get('order/invoice', 'SiteController@orderInvoice')->name('order.invoice');

// Courier routes go here
Route::get('couriers', 'CarrierController@filter')->name('couriers.show');

// Order routes go here
Route::get('orders/complete', 'OrderController@complete')->name('orders.complete');

// Admin routes go here
Route::prefix('secure/administrator/')->group(function () {
    Route::get('admins', 'Admin\AdminController@admins')->name('admins.index');
    Route::get('admins/create', 'Admin\AdminController@create')->name('admins.create');
    Route::post('admins', 'Admin\AdminController@store')->name('admins.store');
    Route::get('admins/{admin}/edit', 'Admin\AdminController@edit')->name('admins.edit');
    Route::put('admins/{admin}', 'Admin\AdminController@update')->name('admins.update');
    Route::delete('admins/{admin}', 'Admin\AdminController@destroy')->name('admins.destroy');
    Route::get('admin/dashboard', 'Admin\AdminController@index')->name('admin.dashboard');
    Route::get('admin/settings', 'Admin\AdminController@settings')->name('admin.settings');
    Route::put('admin/email/update', 'Admin\AdminController@updateEmail')->name('admin.email.update');
    Route::put('admin/password/reset', 'Admin\AdminController@updatePassword')->name('admin.password.reset');
    Route::put('admin/account/update/{admin}', 'Admin\AdminController@updateAdmin')->name('admin.account.update');
    Route::get('admin/login', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('admin/login', 'Admin\Auth\LoginController@login');
    Route::post('admin/logout', 'Admin\Auth\LoginController@logout')->name('admin.logout');

    // User routes go here
    Route::get('users', 'User\UserController@users')->name('users');
    Route::get('users/{user}', 'User\UserController@show')->name('users.show');
    Route::get('users/{user}/edit', 'User\UserController@edit')->name('users.edit');
    Route::put('users-update/{user}', 'User\UserController@updateUsers')->name('users.profile.update');

    // Role routes go here
    Route::resource('roles', 'Admin\RoleController');

    // Custom pages routes go here
    Route::resource('pages', 'PageController');

    // Type routes go here
    Route::resource('types', 'TypeController');

    // Invoice routes go here
    Route::get('invoices', 'InvoiceController@index')->name('invoices.index');
    Route::post('invoices', 'InvoiceController@store')->name('invoices.store');
    Route::get('invoices/{invoice}', 'InvoiceController@show')->name('invoices.show');
    Route::delete('invoices/{invoice}', 'InvoiceController@destroy')->name('invoices.destroy');

    // Order routes go here
    Route::get('orders/complete', 'OrderController@complete')->name('orders.complete');
    Route::resource('orders', 'OrderController');

    // Rate routes go here
    Route::resource('rates', 'RateController');

    // Courier routes go here
    Route::get('carriers/download', function () {
        return Excel::download(new \App\Exports\CarrierExport, 'carriers.xlsx');
    })->name('carriers.download')->middleware('auth:admin');
    Route::resource('carriers', 'CarrierController');
});

// Custom page route goes here
Route::get('/{slug}', 'PageController@slug')->name('page.show');

