<?php

namespace App\Exports;

use App\Carrier;
use App\Traits\Excludable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class CarrierExport implements FromCollection, WithHeadings
{
    use Excludable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Carrier::exclude('logo')->get();
    }

    public function headings(): array
    {
        return [
            'id',
            'title',
            'app_key',
            'secret_key',
            'created_at',
            'updated_at',
        ];
    }
}
