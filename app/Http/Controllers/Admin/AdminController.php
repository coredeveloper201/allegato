<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('super.admin')->except('index', 'updatePassword', 'updateEmail', 'updateAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Get latest registered users
        $clients = User::orderBy('id', 'desc')->take(10)->get();

        // Get total users number
        $total = User::count();

        // Show the admin dashboard
        return view('admin.index', compact('clients', 'total'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admins()
    {
        $admins = Admin::all()->sortByDesc('id');

        return view('admin.admin.admins', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Get all roles
        $roles = Role::all()->sortByDesc('id');

        return view('admin.admin.create-edit-admin', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate form data
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:admins'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role_id' => ['required', 'integer'],
            'avatar' => ['nullable', 'image'],
        ]);

        // Create a new model instance assign form-data then save to DB
        $admin = new Admin();

        // Upload image
        if ($request->hasFile('avatar')) {
            //get filename with extension
            $filenamewithextension = $request->file('avatar')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('avatar')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
            $request->file('avatar')->storeAs('public/images/profile_images/thumbnail', $filenametostore);

            //Resize image here
            $thumbnailpath = public_path('storage/images/profile_images/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(600, 600, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);

            // Assign to model instance
            $admin->avatar = $filenametostore;
        }

        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->password = Hash::make($request->password);
        $admin->save();

        // Insert associated role of admin
        $admin->roles()->attach($request->role_id);

        return redirect()->back()->with('success', 'Admin created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        // Get the model & show data to edit form
        $admin = Admin::find($admin->id);
        $edit = true;

        // Get all roles
        $roles = Role::all()->sortByDesc('id');

        return view('admin.admin.create-edit-admin', compact('admin', 'edit', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        // Validate form data
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['nullable', 'string', 'email', 'max:255', 'unique:admins'],
            'password' => ['nullable', 'string', 'min:8'],
            'role_id' => ['required', 'integer'],
            'avatar' => ['nullable', 'image'],
        ]);

        // Get the model assign form data & save to DB
        $admin = Admin::find($admin->id);

        // Upload image
        if ($request->hasFile('avatar')) {
            //get filename with extension
            $filenamewithextension = $request->file('avatar')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('avatar')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
            $request->file('avatar')->storeAs('public/images/profile_images/thumbnail', $filenametostore);

            //Resize image here
            $thumbnailpath = public_path('storage/images/profile_images/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(600, 600, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);

            // Delete previous image from directory
            if ($admin->avatar) {
                Storage::delete('public/images/profile_images/thumbnail/'.$admin->avatar);
            }

            // Assign to model instance
            $admin->avatar = $filenametostore;
        }

        $admin->name = $request->name;
        if (isset($request->email)) {
            $admin->email = $request->email;
        }
        if (isset($request->password)) {
            $admin->password = Hash::make($request->password);
        }
        $admin->save();

        foreach ($admin->roles as $admin_role) {

        }

        if ($admin_role->id != $request->role_id) {
            $admin->roles()->detach();
            // Insert associated role of admin
            $admin->roles()->attach($request->role_id);
        }

        return redirect()->back()->with('success', 'Admin updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        $admin = Admin::find($admin->id);

        // Delete previous image from directory
        if ($admin->avatar) {
            Storage::delete('public/images/profile_images/thumbnail/'.$admin->avatar);
        }

        $admin->delete();

        return redirect()->back()->with('success', 'Admin deleted successfully.');
    }

    /**
     * Update client password.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request)
    {
        // Validate form data
        $request->validate(array(
            'password' => 'required|string|min:8',
            'new_password' => 'required|string|min:8|confirmed',
        ));

        // Get the admin
        $admin = Admin::find(auth()->user()->id);
        if (!Hash::check($request->password, $admin->password)){
            // Return json response
            return redirect()->back()->with('error', 'Your old password did not match!');
        }

        // Upadate password
        $admin->password = Hash::make($request->new_password);
        $admin->save();

        // Return json response
        return redirect()->back()->with('success', 'Your password updated successfully.');
    }

    /**
     * Update client email.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updateEmail(Request $request)
    {
        // Validate form data
        $request->validate(array(
            'old_email' => 'required|string|email|max:255',
            'email' => 'required|string|email|max:255|confirmed|unique:admins',
        ));

        // Get the admin
        $admin = Admin::find(auth()->user()->id);
        if ($admin->email != $request->old_email){
            // Return json response
            return redirect()->back()->with('error_email', 'Your old email did not match!');
        }

        // Upadate password
        $admin->email = $request->email;
        $admin->save();

        // Return json response
        return redirect()->back()->with('success', 'Your email updated successfully.');
    }

    /**
     * Show the admin settings page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function settings()
    {
        // Get the admin
        $admin = Admin::find(auth()->user()->id);

        return view('admin.admin.account-settings', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function updateAdmin(Request $request, Admin $admin)
    {
        // Validate form data
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'avatar' => ['nullable', 'image'],
        ]);

        // Get the model assign form data & save to DB
        $admin = Admin::find($admin->id);

        // Upload image
        if ($request->hasFile('avatar')) {
            //get filename with extension
            $filenamewithextension = $request->file('avatar')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('avatar')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
            $request->file('avatar')->storeAs('public/images/profile_images/thumbnail', $filenametostore);

            //Resize image here
            $thumbnailpath = public_path('storage/images/profile_images/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(600, 600, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);

            // Delete previous image from directory
            if ($admin->avatar) {
                Storage::delete('public/images/profile_images/thumbnail/'.$admin->avatar);
            }

            // Assign to model instance
            $admin->avatar = $filenametostore;
        }

        $admin->name = $request->name;
        $admin->save();

        return redirect()->back()->with('success', 'Your account updated successfully.');
    }
}
