<?php

namespace App\Http\Controllers;

use App\Carrier;
use Illuminate\Http\Request;
use App\Imports\CarrierImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class CarrierController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin')->except('filter');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all resources
        $carriers = Carrier::all()->sortByDesc('id');

        return view('admin.carrier.carriers', compact('carriers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Show create form
        return view('admin.carrier.carrier-create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('carriers')) {
            // Validate form data
            $request->validate([
                'carriers' => 'required|mimes:csv,xlsx,xls|max:2000',
            ]);

            // Insert data to DB from excel file
            Excel::import(new CarrierImport(), $request->file('carriers'));

            return redirect()->back()->with('success', 'Courier created successfully.');
        }

        // Validate form data
        $request->validate([
            'title' => 'required|string|max:255',
            'app_key' => 'required|string|max:255',
            'secret_key' => 'required|string|max:255',
            'logo' => 'required|image|max:100',
        ]);

        // Upload image
        if ($request->file('logo')->isValid()) {
            //get filename with extension
            $filenamewithextension = $request->file('logo')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('logo')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
            $request->file('logo')->storeAs('public/images/logos/', $filenametostore);
        }

        // Create a new model instance assign form-data then save to DB
        $carrier = new Carrier();
        $carrier->title = $request->title;
        $carrier->logo = $filenametostore;
        $carrier->app_key = $request->app_key;
        $carrier->secret_key = $request->secret_key;
        $carrier->save();

        return redirect()->back()->with('success', 'Courier created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Carrier  $carrier
     * @return \Illuminate\Http\Response
     */
    public function show(Carrier $carrier)
    {
        // Find the model
        $carrier = Carrier::find($carrier->id);

        return view('admin.carrier.carrier-show', compact('carrier'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Carrier  $carrier
     * @return \Illuminate\Http\Response
     */
    public function edit(Carrier $carrier)
    {
        // Find the model
        $carrier = Carrier::find($carrier->id);
        $edit = true;

        return view('admin.carrier.carrier-create-edit', compact('carrier', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Carrier  $carrier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Carrier $carrier)
    {
        // Validate form data
        $request->validate([
            'title' => 'required|string|max:255',
            'app_key' => 'required|string|max:255',
            'secret_key' => 'required|string|max:255',
            'logo' => 'nullable|image|max:100',
        ]);

        // Find the model
        $carrier = Carrier::find($carrier->id);

        // Upload image
        if ($request->hasFile('logo')) {
            //get filename with extension
            $filenamewithextension = $request->file('logo')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('logo')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;

            // Delete previous image from the directory
            Storage::delete('public/images/logos/'.$carrier->logo);

            //Upload File
            $request->file('logo')->storeAs('public/images/logos/', $filenametostore);

            // Assign to model instance
            $carrier->logo = $filenametostore;
        }

        $carrier->title = $request->title;
        $carrier->app_key = $request->app_key;
        $carrier->secret_key = $request->secret_key;
        $carrier->save();

        return redirect()->back()->with('success', 'Courier updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Carrier  $carrier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Carrier $carrier)
    {
        // Find the model
        $carrier = Carrier::find($carrier->id);

        // Delete image from the directory
        Storage::delete('public/images/logos/'.$carrier->logo);

        $carrier->delete();

        return redirect()->back()->with('success', 'Courier removed successfully.');
    }

    /**
     * Display the specified list of resources.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {

    }
}
