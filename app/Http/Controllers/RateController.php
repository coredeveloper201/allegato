<?php

namespace App\Http\Controllers;

use App\Rate;
use App\Type;
use App\Carrier;
use Illuminate\Http\Request;

class RateController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rates = Rate::all()->sortByDesc('id');

        return view('admin.rate.rates', compact('rates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Get all couriers
        $carriers = Carrier::all()->sortByDesc('id');

        // Get all couriers
        $types = Type::all()->sortByDesc('id');

        return view('admin.rate.rate-create-edit', compact('carriers', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate form data
        $request->validate([
            'distance_from' => 'required|numeric|min:0',
            'distance_to' => 'required|numeric|min:0|gte:distance_from',
            'weight_from' => 'required|numeric|min:0',
            'weight_to' => 'required|numeric|min:0|gte:weight_from',
            'length_from' => 'required|numeric|min:0',
            'length_to' => 'required|numeric|min:0|gte:length_from',
            'width_from' => 'required|numeric|min:0',
            'width_to' => 'required|numeric|min:0|gte:width_from',
            'height_from' => 'required|numeric|min:0',
            'height_to' => 'required|numeric|min:0|gte:height_from',
            'price' => 'required|numeric|gt:0',
            'type_id' => 'required|integer',
            'carrier_id' => 'required|integer',
        ]);

        // Create a model instance assign form data & save to DB
        $rate = new Rate();
        $rate->distance_from = $request->distance_from;
        $rate->distance_to = $request->distance_to;
        $rate->weight_from = $request->weight_from;
        $rate->weight_to = $request->weight_to;
        $rate->length_from = $request->length_from;
        $rate->length_to = $request->length_to;
        $rate->width_from = $request->width_from;
        $rate->width_to = $request->width_to;
        $rate->height_from = $request->height_from;
        $rate->height_to = $request->height_to;
        $rate->price = $request->price;
        $rate->type_id = $request->type_id;
        $rate->carrier_id = $request->carrier_id;
        $rate->save();

        return redirect()->back()->with('success', 'Rate created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function show(Rate $rate)
    {
        // Get the model
        $rate = Rate::find($rate->id);

        return view('admin.rate.rate-show', compact('rate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function edit(Rate $rate)
    {
        // Get all couriers
        $carriers = Carrier::all()->sortByDesc('id');

        // Get all couriers
        $types = Type::all()->sortByDesc('id');

        // Get the model
        $rate = Rate::find($rate->id);

        $edit = true;

        return view('admin.rate.rate-create-edit', compact('rate', 'carriers', 'types', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rate $rate)
    {
        // Validate form data
        $request->validate([
            'distance_from' => 'required|numeric|min:0',
            'distance_to' => 'required|numeric|min:0|gte:distance_from',
            'weight_from' => 'required|numeric|min:0',
            'weight_to' => 'required|numeric|min:0|gte:weight_from',
            'length_from' => 'required|numeric|min:0',
            'length_to' => 'required|numeric|min:0|gte:length_from',
            'width_from' => 'required|numeric|min:0',
            'width_to' => 'required|numeric|min:0|gte:width_from',
            'height_from' => 'required|numeric|min:0',
            'height_to' => 'required|numeric|min:0|gte:height_from',
            'price' => 'required|numeric|gt:0',
            'type_id' => 'required|integer',
            'carrier_id' => 'required|integer',
        ]);

        // Get the model assign form data & save to DB
        $rate = Rate::find($rate->id);
        $rate->distance_from = $request->distance_from;
        $rate->distance_to = $request->distance_to;
        $rate->weight_from = $request->weight_from;
        $rate->weight_to = $request->weight_to;
        $rate->length_from = $request->length_from;
        $rate->length_to = $request->length_to;
        $rate->width_from = $request->width_from;
        $rate->width_to = $request->width_to;
        $rate->height_from = $request->height_from;
        $rate->height_to = $request->height_to;
        $rate->price = $request->price;
        $rate->type_id = $request->type_id;
        $rate->carrier_id = $request->carrier_id;
        $rate->save();

        return redirect()->back()->with('success', 'Rate updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rate $rate)
    {
        // Get the model
        Rate::destroy($rate->id);

        return redirect()->back()->with('success', 'Rate deleted successfully.');
    }
}
