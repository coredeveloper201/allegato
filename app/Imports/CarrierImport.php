<?php

namespace App\Imports;

use App\Carrier;
use Maatwebsite\Excel\Concerns\ToModel;

class CarrierImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Carrier([
            'title'     => $row[0],
            'logo'    => $row[1],
            'app_key' => $row[2],
            'secret_key' => $row[3],
        ]);
    }
}
